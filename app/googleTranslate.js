const translate = require("@vitalets/google-translate-api");

const fs = require("fs");
// eslint-disable-next-line camelcase
const path_module = require("path");
const argv = require("minimist")(process.argv.slice(2));

const path = "src/locales";
const report = argv.r;

const LOCALES_CONFIG = initLocalesConfig(path, argv.l);

parseReport(LOCALES_CONFIG).then((r) => {
  LOCALES_CONFIG.forEach((locale) => {
    console.log(locale.file);
    write(locale.path, locale.file);
  });
});

function getLocalePath(mainPath, lang) {
  console.log(
    path_module.join(__dirname, mainPath, `${lang}/translation.json`),
  );
  return path_module.join(__dirname, mainPath, `${lang}/translation.json`);
}

function openFileByPath(path) {
  return require(path);
}

function initLocalesConfig(path, langs) {
  const langsArray = typeof langs === "string" ? [langs] : langs;
  return langsArray.map((lang) => {
    const filePath = getLocalePath(path, lang);
    return {
      path: filePath,
      file: openFileByPath(filePath),
      isMainLang: lang === "en",
    };
  });
}

function write(path, file) {
  fs.writeFile(path, JSON.stringify(file, null, 4), (err) => {
    if (err) {
      console.error(file, err);
    }
  });
}

function unlink(path) {
  fs.unlink(path, (err) => {
    if (err) {
      console.error(err);
    }
  });
}

async function translateKey(key) {
  try {
    const res = await translate(key, { to: "en" });
    return res.text;
  } catch (e) {
    console.error(err);
  }
}

async function parseReport(config) {
  for (const lang of config) {
    for (const [key, value] of Object.entries(lang.file)) {
      lang.file[key] = await translateKey(key);
    }
  }
}
