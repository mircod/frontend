export interface User {
  id: number;
  name: string;
  temperature: number | null;
  temp_date: string | null;
  device_id: string | null;
  charge: number | null;
  check?: boolean;
}

export interface PayloadUserCheck {
  id: number;
}

export interface PayloadCheckAll {
  flag: boolean;
}
