export interface Modal extends ModalContext, ModalOpen {
  component: null | string;
}

export interface ModalContext {
  context: null | string | object;
}

export interface ModalClose {
  open: boolean;
}

export interface ModalOpen {
  open: boolean;
  component: string;
}

export interface ModalSelector {
  component: null | string;
  context: null | string | object;
  open: boolean;
}
