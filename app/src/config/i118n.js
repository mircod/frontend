import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import en from "../locales/en/translation.json";
import ru from "../locales/ru/translation.json";
import BrowserLanguage from "../utils/BrowserLanguage";
import { IS_PRODUCTION } from "../constants";

const CURRENT_LANG = BrowserLanguage.getDefaultLanguage();

const resources = {
  en: {
    translation: en,
  },
  ru: {
    translation: ru,
  },
};

i18n.use(initReactI18next).init({
  resources,
  ns: ["translation"],
  fallbackLng: "ru",
  interpolation: {
    escapeValue: false,
  },
  lng: CURRENT_LANG,
  debug: IS_PRODUCTION,
});

export default i18n;
