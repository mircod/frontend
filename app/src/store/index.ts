import { configureStore } from "@reduxjs/toolkit";
import users from "~/store/slices/users";
import modal from "~/store/slices/modal";
import user from "~/store/slices/user";

const store = configureStore({
  reducer: {
    users,
    modal,
    user,
  },
});

export type RootState = ReturnType<typeof store.getState>;
// @ts-ignore
export type AppStore = ReturnType<typeof store>;
export type AppDispatch = AppStore["dispatch"];

export default store;
