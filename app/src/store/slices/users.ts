import { createSlice, PayloadAction, Slice } from "@reduxjs/toolkit";
import { RootState } from "~/store";
import { PayloadCheckAll, PayloadUserCheck, User } from "~/appTypes/users";

interface Info {
  id: number;
  name: string;
}

interface InitialState {
  users: User[];
}

const initialState: InitialState = {
  users: [
    {
      id: 1,
      name: "",
      temperature: 38,
      temp_date: "27 июля 2021 г. 10:57",
      device_id: "",
      charge: 35,
      check: false,
    },
  ],
};

const usersSlice: Slice = createSlice({
  name: "users",
  initialState,
  reducers: {
    setCheckUser(
      state: InitialState,
      { payload }: PayloadAction<PayloadUserCheck>,
    ) {
      const { id } = payload;

      const userCheck = state.users.find((item) => item.id === id);
      if (userCheck) {
        userCheck.check = !userCheck.check;
      }
    },
    setCheckAll(
      state: InitialState,
      { payload }: PayloadAction<PayloadCheckAll>,
    ) {
      const { flag } = payload;
      state.users.forEach((item) => {
        item.check = flag;
      });
    },
    setUsers(state: InitialState, { payload }: PayloadAction<User[]>) {
      state.users = payload;
    },
  },
});

const { reducer, actions } = usersSlice;

export const { setCheckUser, setCheckAll, setUsers } = actions;

export const selectGetUsers = (state: RootState) => state.users.users;
export const selectIsEmpty = (state: RootState) => state.users.users.length > 0;
export const selectCheckCounter = (state: RootState) => {
  let counter = 0;
  const info: Info[] = [];

  state.users.users.forEach((item: User) => {
    if (item.check) {
      info.push({ id: item.id, name: item.name });
    }
  });

  counter = info.length;

  return { counter, info };
};
export const selectIsCheckAll = (state: RootState) => {
  return state.users.users.every((item: User) => item.check === true);
};

export default reducer;
