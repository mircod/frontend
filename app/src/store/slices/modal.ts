import { createSlice, PayloadAction, Slice } from "@reduxjs/toolkit";
import {
  Modal,
  ModalClose,
  ModalContext,
  ModalOpen,
  ModalSelector,
} from "~/appTypes/modal";
import { RootState } from "~/store";

const initialState: Modal = {
  open: false,
  component: null,
  context: null,
};

const modalSlice: Slice = createSlice({
  name: "modal",
  initialState,
  reducers: {
    setOpen(state: Modal, { payload }: PayloadAction<ModalOpen>) {
      const { open, component } = payload;
      state.open = open;
      state.component = component;
    },
    setContext(state, { payload }: PayloadAction<ModalContext>) {
      const { context } = payload;
      state.context = context;
    },
    clearModal(state: Modal, { payload }: PayloadAction<ModalClose>) {
      const { open } = payload;
      state.open = open;
      state.component = null;
      state.context = null;
    },
  },
});

const { reducer, actions } = modalSlice;

export const { setOpen, setContext, clearModal } = actions;

export const selectComponentModal = (state: RootState): ModalSelector => {
  return {
    component: state.modal.component,
    context: state.modal.context,
    open: state.modal.open,
  };
};

export default reducer;
