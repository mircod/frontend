import { createSlice, PayloadAction, Slice } from "@reduxjs/toolkit";
import { User } from "~/appTypes/users";
import { RootState } from "~/store";
import { Measurement } from "~/api/generated";

interface InitialState {
  user: User;
  temp: Measurement[];
}

const initialState: InitialState = {
  user: null,
  temp: [],
};

const userSlice: Slice = createSlice({
  name: "user",
  initialState,
  reducers: {
    setUser(state, { payload }: PayloadAction<User>) {
      state.user = payload;
    },
    setTemp(state, { payload }: PayloadAction<Measurement[]>) {
      state.temp = payload;
    },
  },
});

const { reducer, actions } = userSlice;

export const { setUser, setTemp } = actions;

export const selectGetUser = (state: RootState) => state.user.user;
export const selectGetTemp = (state: RootState) => state.user.temp;

export default reducer;
