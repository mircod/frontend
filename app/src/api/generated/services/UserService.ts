/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { BulkDeleteUser } from "../models/BulkDeleteUser";
import type { Measurement } from "../models/Measurement";
import type { Patient } from "../models/Patient";
import type { PatientCreate } from "../models/PatientCreate";

import type { CancelablePromise } from "../core/CancelablePromise";
import { OpenAPI } from "../core/OpenAPI";
import { request as __request } from "../core/request";

export class UserService {
  /**
   * Returns user list
   * @param search A search term.
   * @returns Patient
   * @throws ApiError
   */
  public static userList(search?: string): CancelablePromise<Array<Patient>> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/user/",
      query: {
        search: search,
      },
    });
  }

  /**
   * Create patient
   * @param data
   * @returns PatientCreate
   * @throws ApiError
   */
  public static userCreate(
    data: PatientCreate,
  ): CancelablePromise<PatientCreate> {
    return __request(OpenAPI, {
      method: "POST",
      url: "/user/",
      body: data,
    });
  }

  /**
   * Delete a few users
   * @param data
   * @returns void
   * @throws ApiError
   */
  public static userBulkDelete(data: BulkDeleteUser): CancelablePromise<void> {
    return __request(OpenAPI, {
      method: "DELETE",
      url: "/user/bulk_delete/",
      body: data,
    });
  }

  /**
   * Get info about user
   * @param id A unique integer value identifying this User.
   * @returns Patient
   * @throws ApiError
   */
  public static userRead(id: number): CancelablePromise<Patient> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/user/{id}/",
      path: {
        id: id,
      },
    });
  }

  /**
   * Update user info with sending all fields
   * @param id A unique integer value identifying this User.
   * @param data
   * @returns Patient
   * @throws ApiError
   */
  public static userUpdate(
    id: number,
    data: Patient,
  ): CancelablePromise<Patient> {
    return __request(OpenAPI, {
      method: "PUT",
      url: "/user/{id}/",
      path: {
        id: id,
      },
      body: data,
    });
  }

  /**
   * Update user info
   * @param id A unique integer value identifying this User.
   * @param data
   * @returns Patient
   * @throws ApiError
   */
  public static userPartialUpdate(
    id: number,
    data: Patient,
  ): CancelablePromise<Patient> {
    return __request(OpenAPI, {
      method: "PATCH",
      url: "/user/{id}/",
      path: {
        id: id,
      },
      body: data,
    });
  }

  /**
   * Delete user
   * @param id A unique integer value identifying this User.
   * @returns void
   * @throws ApiError
   */
  public static userDelete(id: number): CancelablePromise<void> {
    return __request(OpenAPI, {
      method: "DELETE",
      url: "/user/{id}/",
      path: {
        id: id,
      },
    });
  }

  /**
   * Get list of user temperature measurements
   * @param userPk
   * @returns Measurement
   * @throws ApiError
   */
  public static userTempList(
    userPk: string,
  ): CancelablePromise<Array<Measurement>> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/user/{user_pk}/temp/",
      path: {
        user_pk: userPk,
      },
    });
  }
}
