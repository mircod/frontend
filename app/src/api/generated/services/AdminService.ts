/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { RegisterUserResponse } from "../models/RegisterUserResponse";
import type { Registration } from "../models/Registration";

import type { CancelablePromise } from "../core/CancelablePromise";
import { OpenAPI } from "../core/OpenAPI";
import { request as __request } from "../core/request";

export class AdminService {
  /**
   * Create admin.
   * @param data
   * @returns RegisterUserResponse
   * @throws ApiError
   */
  public static adminCreate(
    data: Registration,
  ): CancelablePromise<RegisterUserResponse> {
    return __request(OpenAPI, {
      method: "POST",
      url: "/admin/",
      body: data,
    });
  }
}
