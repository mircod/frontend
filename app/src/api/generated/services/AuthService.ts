/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Login } from "../models/Login";
import type { LoginResponse } from "../models/LoginResponse";
import type { RegisterUserResponse } from "../models/RegisterUserResponse";
import type { Registration } from "../models/Registration";

import type { CancelablePromise } from "../core/CancelablePromise";
import { OpenAPI } from "../core/OpenAPI";
import { request as __request } from "../core/request";

export class AuthService {
  /**
   * Sign in.
   * @param data
   * @returns LoginResponse
   * @throws ApiError
   */
  public static authLoginCreate(data: Login): CancelablePromise<LoginResponse> {
    return __request(OpenAPI, {
      method: "POST",
      url: "/auth/login/",
      body: data,
    });
  }

  /**
   * Sign up.
   * @param data
   * @returns RegisterUserResponse
   * @throws ApiError
   */
  public static authRegisterCreate(
    data: Registration,
  ): CancelablePromise<RegisterUserResponse> {
    return __request(OpenAPI, {
      method: "POST",
      url: "/auth/register/",
      body: data,
    });
  }
}
