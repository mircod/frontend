/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Device } from "../models/Device";
import type { Measurement } from "../models/Measurement";

import type { CancelablePromise } from "../core/CancelablePromise";
import { OpenAPI } from "../core/OpenAPI";
import { request as __request } from "../core/request";

export class DeviceService {
  /**
   * Returns list of devices
   * @returns Device
   * @throws ApiError
   */
  public static deviceList(): CancelablePromise<Array<Device>> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/device/",
    });
  }

  /**
   * Create device
   * @param data
   * @returns Device
   * @throws ApiError
   */
  public static deviceCreate(data: Device): CancelablePromise<Device> {
    return __request(OpenAPI, {
      method: "POST",
      url: "/device/",
      body: data,
    });
  }

  /**
   * Save measurement data from device
   * @param devicePk
   * @param data
   * @returns Measurement
   * @throws ApiError
   */
  public static deviceTempCreate(
    devicePk: string,
    data: Measurement,
  ): CancelablePromise<Measurement> {
    return __request(OpenAPI, {
      method: "POST",
      url: "/device/{device_pk}/temp/",
      path: {
        device_pk: devicePk,
      },
      body: data,
    });
  }

  /**
   * Returns info about device
   * @param id A unique integer value identifying this Устройство.
   * @returns Device
   * @throws ApiError
   */
  public static deviceRead(id: number): CancelablePromise<Device> {
    return __request(OpenAPI, {
      method: "GET",
      url: "/device/{id}/",
      path: {
        id: id,
      },
    });
  }

  /**
   * Update info about device
   * @param id A unique integer value identifying this Устройство.
   * @param data
   * @returns Device
   * @throws ApiError
   */
  public static deviceUpdate(
    id: number,
    data: Device,
  ): CancelablePromise<Device> {
    return __request(OpenAPI, {
      method: "PUT",
      url: "/device/{id}/",
      path: {
        id: id,
      },
      body: data,
    });
  }

  /**
   * @param id A unique integer value identifying this Устройство.
   * @param data
   * @returns Device
   * @throws ApiError
   */
  public static devicePartialUpdate(
    id: number,
    data: Device,
  ): CancelablePromise<Device> {
    return __request(OpenAPI, {
      method: "PATCH",
      url: "/device/{id}/",
      path: {
        id: id,
      },
      body: data,
    });
  }
}
