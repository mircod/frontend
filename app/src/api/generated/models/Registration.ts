/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Registration = {
  username: string;
  password: string;
  confirm_password: string;
};
