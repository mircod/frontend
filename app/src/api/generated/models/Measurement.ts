/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Measurement = {
  readonly id?: number;
  value: number;
  date: string;
  readonly device?: number;
  readonly user?: number;
};
