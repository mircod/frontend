/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type PatientCreate = {
  first_name: string;
  last_name: string;
  /**
   * Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.
   */
  username: string;
  birth_date?: string | null;
};
