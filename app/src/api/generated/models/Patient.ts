/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type Patient = {
  readonly id?: number;
  readonly device_id?: string;
  readonly name?: string;
  readonly temperature?: string;
  readonly temp_date?: string;
  readonly charge?: string;
};
