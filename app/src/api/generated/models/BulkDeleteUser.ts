/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type BulkDeleteUser = {
  id: Array<number>;
};
