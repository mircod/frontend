import { OpenAPI } from "~/api/generated";
import { API_URL } from "~/constants";
import { useLocation, useNavigate } from "react-router-dom";

const getToken = () => {
  if (localStorage.getItem("token")) {
    return localStorage.getItem("token");
  } else {
    return null;
  }
};

export const isHaveToken = () => {
  return !!localStorage.getItem("token");
};

const navigateLogin = () => {
  const navigate = useNavigate();
  const location = useLocation();
  if (localStorage.getItem("token")) {
    return null;
  } else {
    if (location.pathname === "/login" || location.pathname === "/sign_up") {
      return null;
    }
    return navigate("/login");
  }
};

export function apiInit() {
  if (getToken() !== null) {
    const myHeaderObject = {
      Authorization: `Token ${getToken()}`,
    };

    // @ts-ignore
    OpenAPI.HEADERS = { ...myHeaderObject };
  }
  OpenAPI.BASE = API_URL;

  navigateLogin();
}
