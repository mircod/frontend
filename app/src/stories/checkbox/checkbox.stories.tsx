import { useState } from "react";
// @ts-ignore
import CheckBox from "~/components/UI/CheckBox";

export default {
  title: "Checkbox",
  component: CheckBox,
};

export const CheckedCheckbox = () => {
  const [value, setValue] = useState(false);
  const checkAll = () => {
    setValue(!value);
  };
  return <CheckBox value={value} dark check={checkAll} />;
};
