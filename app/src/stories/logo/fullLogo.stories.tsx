// eslint-disable-next-line import/no-useless-path-segments
import FullLogo from "~/components/UI/logo/FullLogo";

export default {
  title: "Logo",
  component: FullLogo,
};

export const GetFullLogo = () => <FullLogo />;
