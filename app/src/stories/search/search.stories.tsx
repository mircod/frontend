// eslint-disable-next-line import/no-useless-path-segments
import Search from "~/components/UI/search/Search";

export default {
  title: "Search",
  component: Search,
};

export const GetSearch = () => (
  <Search type="text" name="search" placeholder="Search" />
);
