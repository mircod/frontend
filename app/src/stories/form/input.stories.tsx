// eslint-disable-next-line import/no-useless-path-segments
import Input from "~/components/UI/form/Input";
// eslint-disable-next-line no-unused-vars
import Form from "~/components/UI/form/Form";

export default {
  title: "Input",
  component: Input,
};

export const GetInput = () => (
  <Form>
    <Input
      label="Имя пользователя"
      labelFor="userName"
      type="text"
      id="userName"
      name="userName"
      placeholder="Екатерина"
    />
  </Form>
);
