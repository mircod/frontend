import { Outlet } from "react-router-dom";
import { FC } from "react";

import Header from "~/components/header/Header";
import Container from "~/components/Container";

const MainLayout: FC = (): JSX.Element => {
  return (
    <div className="wrapper">
      <Header />
      <Container>
        <Outlet />
      </Container>
    </div>
  );
};

export default MainLayout;
