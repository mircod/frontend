import { FC } from "react";
import { useParams } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";
import CardTable from "~/components/userCard/table/CardTable";
import CardBack from "~/components/userCard/CardBack";
import CardInfo from "~/components/userCard/CardInfo";
import DeviceTempDate from "~/components/userCard/device/DeviceTempDate";
import ChartTemp from "~/components/UI/charts/ChartTemp";
import useUser, { useUserTemp } from "~/hooks/useUser";
import EmptyResult from "~/components/UI/empty/EmptyResult";
import { TableSkeletonThree } from "~/components/UI/skeleton/TablesSkeleton";
import { selectGetUser } from "~/store/slices/user";
import { dateFormatter } from "~/utils/dateFormatter";

const UserCard: FC = () => {
  const { t } = useTranslation();
  const { id } = useParams();
  const { isLoading: isLoadingUser } = useUser(parseInt(id, 10));
  const { data: dataTemp, isLoading } = useUserTemp(id);
  const data = useSelector(selectGetUser);
  return (
    <div>
      <CardBack />
      {isLoadingUser ? (
        ""
      ) : (
        <>
          {data.temp_date === "" ? (
            ""
          ) : (
            <DeviceTempDate date={dateFormatter(data.temp_date)} />
          )}
          <CardInfo />
        </>
      )}
      {/* eslint-disable-next-line no-nested-ternary */}
      {isLoading ? (
        <TableSkeletonThree />
      ) : dataTemp.length > 0 ? (
        <>
          <ChartTemp />
          <CardTable />
        </>
      ) : (
        <EmptyResult text={t("Нет данных за этот день")} />
      )}
    </div>
  );
};

export default UserCard;
