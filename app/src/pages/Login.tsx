import { FC } from "react";
import { useFormik } from "formik";
import { Link, useNavigate } from "react-router-dom";

import { useTranslation } from "react-i18next";
import { useMutation } from "react-query";
import style from "~/styles/modules/login/login.module.scss";

import Container from "~/components/Container";
import Form from "~/components/UI/form/Form";
import Input from "~/components/UI/form/Input";
import Button from "~/components/UI/button/Button";
import {
  AuthService,
  LoginResponse,
  Login as LoginModel,
} from "~/api/generated";
import { validationLoginSchema } from "~/utils/validationPost";

const Login: FC = () => {
  const navigate = useNavigate();
  const { mutate, isLoading } = useMutation(
    ["login"],
    (data: LoginModel) => AuthService.authLoginCreate(data),
    {
      onSuccess: (token: LoginResponse) => {
        localStorage.setItem("token", token.token);
        navigate("/");
      },
    },
  );

  const formik = useFormik({
    initialValues: {
      username: "",
      password: "",
    },
    onSubmit: (values) => {
      mutate(values);
    },
    validateOnBlur: true,
    validationSchema: validationLoginSchema,
  });

  const { t } = useTranslation();

  return (
    <main className={style.login}>
      <Container>
        <div className={style.login__logo}>
          <img src="./images/icons/logo.png" alt="" />
        </div>
        <h1 className={style.login__title}>{t("Авторизация")}</h1>
        <Form onSubmit={formik.handleSubmit}>
          <Input
            label={t("Имя пользователя")}
            labelFor="username"
            type="text"
            id="username"
            name="username"
            value={formik.values.username}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          {formik.touched.username && formik.errors.username && (
            <p className={style.error}>{formik.errors.username}</p>
          )}
          <Input
            label={t("Пароль")}
            labelFor="password"
            type="password"
            id="password"
            name="password"
            value={formik.values.password}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          {formik.touched.password && formik.errors.password && (
            <p className={style.error}>{formik.errors.password}</p>
          )}
          <p className={style.account}>
            {t("Нет учетной записи?")}&nbsp;
            <Link to="/sign_up">{t("Создайте ее")}</Link>
          </p>
          <Button
            disabled={!formik.isValid || isLoading}
            isLoading={isLoading}
            type="submit"
            text={t("Войти")}
          />
        </Form>
      </Container>
    </main>
  );
};

export default Login;
