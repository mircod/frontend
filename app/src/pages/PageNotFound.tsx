import { FC } from "react";
import { Link } from "react-router-dom";

import { useTranslation } from "react-i18next";
import style from "~/styles/modules/notFound/notFound.module.scss";

import Container from "~/components/Container";

const PageNotFound: FC = () => {
  const { t } = useTranslation();

  return (
    <main className={style.notFound}>
      <Container>
        <div className={style.notFound__img}>
          <img src="./images/icons/404.png" alt="" />
        </div>
        <h1 className={style.notFound__title}>{t("Страница не найдена")}</h1>
        <p className={style.notFound__text}>
          {t("Контент, который вы ищете, не существует")}.
          {t("Либо он был удален, либо вы неправильно ввели ссылку")}.
        </p>
        <Link to="/" className={style.notFound__button}>
          <span>{t("Назад")}</span>
        </Link>
      </Container>
    </main>
  );
};

export default PageNotFound;
