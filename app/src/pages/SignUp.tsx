import { FC } from "react";
import { useFormik } from "formik";

import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import { useMutation } from "react-query";
import style from "~/styles/modules/login/login.module.scss";
import validationPostSchema from "~/utils/validationPost";

import Container from "~/components/Container";
import Form from "~/components/UI/form/Form";
import Input from "~/components/UI/form/Input";
import Button from "~/components/UI/button/Button";
import CheckBox from "~/components/UI/CheckBox";
import { AuthService, Registration } from "~/api/generated";
import { LOGIN } from "~/constants/route";

const SignUp: FC = () => {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const { mutate, isLoading } = useMutation(
    ["registration"],
    (data: Registration) => AuthService.authRegisterCreate(data),
    {
      onSuccess: () => {
        navigate(LOGIN);
      },
    },
  );

  const formik = useFormik({
    initialValues: {
      username: "",
      password: "",
      confirm_password: "",
      agree: false,
    },
    validateOnBlur: true,
    validationSchema: validationPostSchema,
    onSubmit: (values) => {
      const data = {
        username: values.username,
        password: values.password,
        confirm_password: values.confirm_password,
      };
      mutate(data);
    },
  });

  const changeCheckbox = () => {
    formik.setFieldValue("agree", !formik.values.agree);
    if (formik.values.agree === true) {
      formik.setFieldError("agree", t("Обязательное поле"));
    } else {
      formik.setFieldError("agree", null);
    }
  };

  return (
    <main className={style.login}>
      <Container>
        <div className={style.login__logo}>
          <img src="./images/icons/logo.png" alt="" />
        </div>
        <h1 className={style.login__title}>{t("Регистрация")}</h1>
        <Form onSubmit={formik.handleSubmit}>
          <Input
            label={t("Имя пользователя")}
            labelFor="username"
            type="text"
            id="username"
            name="username"
            value={formik.values.username}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          {formik.touched.username && formik.errors.username && (
            <p className={style.error}>{formik.errors.username}</p>
          )}
          <Input
            label={t("Пароль")}
            labelFor="password"
            type="password"
            id="password"
            name="password"
            value={formik.values.password}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          {formik.touched.password && formik.errors.password && (
            <p className={style.error}>{formik.errors.password}</p>
          )}
          <Input
            label={t("Подтвердите пароль")}
            labelFor="confirm_password"
            type="password"
            id="confirm_password"
            name="confirm_password"
            value={formik.values.confirm_password}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          {formik.touched.confirm_password &&
            formik.errors.confirm_password && (
              <p className={style.error}>{formik.errors.confirm_password}</p>
            )}
          <div className={style.agreement}>
            <CheckBox
              value={formik.values.agree}
              check={() => changeCheckbox()}
              name="agree"
            />
            <span className={style.agreement__text}>
              {t("Соглашаюсь с")}{" "}
              <a href="https://gitlab.com/mircod/frontend">
                {t("Условиями использования")}
              </a>
            </span>
          </div>
          {formik.errors.agree && (
            <p className={style.error}>{formik.errors.agree}</p>
          )}
          <Button
            disabled={!formik.isValid || !formik.values.agree || isLoading}
            type="submit"
            isLoading={isLoading}
            text={t("Зарегистрироваться")}
          />
        </Form>
      </Container>
    </main>
  );
};

export default SignUp;
