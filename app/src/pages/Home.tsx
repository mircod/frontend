import { FC } from "react";
import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";
import UsersTable from "~/components/home/table/UsersTable";
import ModalConfirmDelete from "~/components/home/modal/ModalConfirmDelete";
import UsersHeader from "~/components/home/header/UsersHeader";
import UsersFooter from "~/components/home/footer/UsersFooter";
import EmptyResult from "~/components/UI/empty/EmptyResult";
import { selectIsEmpty } from "~/store/slices/users";

const Home: FC = () => {
  const { t } = useTranslation();
  const isEmpty = useSelector(selectIsEmpty);

  return (
    <div>
      <UsersHeader />
      {!isEmpty ? (
        <EmptyResult text={t("Пока здесь нет ни одного пользователя")} />
      ) : (
        <>
          <UsersTable />
          <UsersFooter />
          <ModalConfirmDelete />
        </>
      )}
    </div>
  );
};

export default Home;
