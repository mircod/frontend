import { FC } from "react";
import clsx from "clsx";
import styles from "~/styles/modules/svg/CheckSvg.module.scss";

interface Props {
  small?: Boolean;
}

const CheckSvg: FC<Props> = ({ small = false }) => {
  return (
    <svg
      className={clsx(styles.CheckSvgIcon, {
        [styles.CheckSvgSmallIcon]: small,
      })}
      width="20"
      height="20"
      viewBox="0 0 20 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M17.2559 4.41074C17.5814 4.73618 17.5814 5.26382 17.2559 5.58925L8.08926 14.7559C7.76382 15.0814 7.23618 15.0814 6.91074 14.7559L2.74408 10.5893C2.41864 10.2638 2.41864 9.73618 2.74408 9.41074C3.06951 9.08531 3.59715 9.08531 3.92259 9.41074L7.5 12.9882L16.0774 4.41074C16.4028 4.0853 16.9305 4.0853 17.2559 4.41074Z"
      />
    </svg>
  );
};

export default CheckSvg;
