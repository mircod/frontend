import { FC, memo } from "react";
import clsx from "clsx";
import style from "~/styles/modules/svg/TrashSvg.module.scss";

interface Props {
  active?: boolean;
}

const TrashSvg: FC<Props> = ({ active }) => {
  return (
    <svg
      width="24"
      height="24"
      viewBox="0 0 24 24"
      xmlns="http://www.w3.org/2000/svg"
      className={clsx(style.Trash, { [style.TrashActive]: active })}
    >
      <path d="M5.25 19.5C5.25 20.875 6.375 22 7.75 22H15.25C16.625 22 17.75 20.875 17.75 19.5V7H5.25V19.5ZM7.75 9.5H15.25V19.5H7.75V9.5ZM14 2H9L7.75 3.25H4V5.75H19V3.25H15.25L14 2Z" />
    </svg>
  );
};

export default memo(TrashSvg);
