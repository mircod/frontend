import { FC, memo } from "react";
import style from "~/styles/modules/svg/DownArrowSvg.module.scss";

const DownArrowSvg: FC = () => {
  return (
    <svg
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      className={style.DownArrowSvg}
    >
      <path
        d="M7.41 8.59L12 13.17L16.59 8.59L18 10L12 16L6 10L7.41 8.59Z"
        fill="#5CA5FA"
      />
    </svg>
  );
};

export default memo(DownArrowSvg);
