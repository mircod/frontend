import { FC, useEffect, useState } from "react";

interface Props {
  charge: number;
}

const ChargeSvg: FC<Props> = ({ charge }) => {
  const fullWidth = 14;
  const [chargeWidth, setCharge] = useState(0);
  const [color, setColor] = useState("#5CA5FA");

  useEffect(() => {
    setCharge((fullWidth * charge) / 100);
    setColor(() => {
      if (charge >= 66) {
        return "#84C299";
      }
      if (charge < 66 && charge > 21) {
        return "#5CA5FA";
      }
      return "#E38888";
    });
  }, [charge]);

  return (
    <svg
      width="24"
      height="24"
      viewBox="0 0 24 24"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M20 15.537V14.7C20 14.1477 20.4477 13.7 21 13.7V13.7C21.5523 13.7 22 13.2523 22 12.7V10.3C22 9.74772 21.5523 9.3 21 9.3V9.3C20.4477 9.3 20 8.85228 20 8.3V7.463C20 6.66 19.4 6 18.67 6L3.34 6C2.6 6 2 6.66 2 7.463V15.526C2 16.34 2.6 17 3.33 17L18.67 17C19.4 17 20 16.34 20 15.537Z"
        stroke={color}
        fill="white"
        strokeWidth="2"
      />
      <rect x="4" y="8" width={chargeWidth} height="7" rx="1" fill={color} />
    </svg>
  );
};

export default ChargeSvg;
