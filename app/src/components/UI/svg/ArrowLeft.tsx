import { FC, memo } from "react";
import styles from "~/styles/modules/svg/ArrowLeft.module.scss";

const ArrowLeft: FC = () => {
  return (
    <svg
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      className={styles.ArrowLeft}
    >
      <path
        d="M15.9405 19.0595C16.4595 18.5405 16.46 17.6991 15.9415 17.1795L10.7733 12L15.9415 6.82051C16.46 6.30091 16.4595 5.45954 15.9405 4.94051C15.4211 4.42108 14.5789 4.42108 14.0595 4.94051L7.70711 11.2929C7.31658 11.6834 7.31658 12.3166 7.70711 12.7071L14.0595 19.0595C14.5789 19.5789 15.4211 19.5789 15.9405 19.0595Z"
        fill="#5CA5FA"
      />
    </svg>
  );
};

export default memo(ArrowLeft);
