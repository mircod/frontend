import { FC } from "react";
import clsx from "clsx";
import { useTranslation } from "react-i18next";
import styles from "~/styles/modules/Dropdown.module.scss";
import SearchSkeleton from "~/components/UI/skeleton/SearchSkeleton";
import { Patient } from "~/api/generated";

interface Props {
  open: boolean;
  isLoading?: boolean;
  listItems: Array<Patient>;
  clickSearch: (item: Patient) => void;
}

const DropdownSearch: FC<Props> = ({
  open,
  isLoading,
  listItems,
  clickSearch,
}) => {
  const { t } = useTranslation();

  const renderList = () => {
    if (listItems.length > 0) {
      return listItems.map((item) => (
        <button
          type="button"
          className={styles.DropdownSearchItem}
          key={item.id}
          onClick={(e) => {
            e.preventDefault();
            clickSearch(item);
          }}
        >
          <div
            className={clsx(
              styles.DropdownSearchText,
              styles.DropdownSearchMargin,
            )}
          >
            {item.name}
          </div>
        </button>
      ));
    }
    return (
      <div className={styles.DropdownSearchItem}>
        <div
          className={clsx(
            styles.DropdownSearchText,
            styles.DropdownSearchMargin,
          )}
        >
          {t("Ничего не найдено")}
        </div>
      </div>
    );
  };

  return (
    <div
      className={clsx(styles.DropdownSearch, {
        [styles.DropdownSearchOpen]: open,
      })}
    >
      <nav className={styles.DropdownSearchNav}>
        {isLoading ? <SearchSkeleton /> : renderList()}
      </nav>
    </div>
  );
};

export default DropdownSearch;
