import { FC, useState } from "react";
import clsx from "clsx";
import { useTranslation } from "react-i18next";
import styles from "~/styles/modules/Dropdown.module.scss";
import CheckBox from "~/components/UI/CheckBox";

interface ListItem {
  item: string;
  check: boolean;
  value: string;
}

type List = ListItem[];

interface Props {
  open: boolean;
}

const Dropdown: FC<Props> = ({ open }) => {
  const { t } = useTranslation();
  const initialList: List = [
    {
      value: "normal",
      item: t("Нормальная"),
      check: false,
    },
    {
      value: "exceeded",
      item: t("Превышена"),
      check: false,
    },
    {
      value: "lowered",
      item: t("Понижена"),
      check: false,
    },
  ];

  const [list, setList] = useState(initialList);

  const checkList = (value: string) => {
    setList(() => {
      return list.map((item: ListItem) => {
        const newItem = { ...item };
        if (newItem.value === value) {
          newItem.check = !newItem.check;
        } else {
          newItem.check = false;
        }
        return newItem;
      });
    });
  };

  return (
    <div className={clsx(styles.Dropdown, { [styles.DropdownOpen]: open })}>
      <div className={styles.DropdownHeader}>
        <div className={styles.DropdownText}>{t("Фильтровать")}:</div>
      </div>
      <nav className={styles.DropdownNav}>
        {list.map((item) => (
          <button
            type="button"
            className={styles.DropdownItem}
            key={item.value}
            onClick={(e) => {
              e.preventDefault();
              checkList(item.value);
            }}
          >
            <CheckBox small value={item.check} />
            <div className={clsx(styles.DropdownText, styles.DropdownMargin)}>
              {item.item}
            </div>
          </button>
        ))}
      </nav>
    </div>
  );
};

export default Dropdown;
