import { FC, memo } from "react";
import styles from "~/styles/modules/Charge.module.scss";
import ChargeSvg from "~/components/UI/svg/ChargeSvg";

interface Props {
  charge: number | null;
}

const Charge: FC<Props> = ({ charge }) => {
  return (
    <div className={styles.Charge}>
      {charge ? <ChargeSvg charge={charge} /> : ""}
      <div className={styles.ChargeText}>{charge ? `${charge}%` : "—"}</div>
    </div>
  );
};

export default memo(Charge);
