const Logo = () => {
  return (
    <div className="logo">
      <img src="./images/icons/logo.png" alt="" />
    </div>
  );
};

export default Logo;
