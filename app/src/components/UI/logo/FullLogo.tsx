const FullLogo = () => {
  return (
    <div className="logo">
      <img src="./images/icons/full-logo.png" alt="" />
    </div>
  );
};

export default FullLogo;
