import { FC, ReactNode } from "react";
import clsx from "clsx";
import { useTranslation } from "react-i18next";
import styles from "~/styles/modules/modal/Modal.module.scss";
import { ModalTitle } from "~/components/UI/modal/ModalText";

interface Props {
  children: ReactNode;
  active: Boolean;
}

const Modal: FC<Props> = ({ children, active }) => {
  const { t } = useTranslation();
  return (
    <div className={clsx(styles.Modal, { [styles.ModalActive]: active })}>
      <div className={styles.ModalFlex}>
        <div className={styles.ModalWindow}>
          <ModalTitle text={t("Удаление пользователя")} />
          {children}
        </div>
      </div>
    </div>
  );
};

export default Modal;
