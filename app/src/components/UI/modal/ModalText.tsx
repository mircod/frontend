import { FC, memo } from "react";
import clsx from "clsx";
// @ts-ignore
import DOMPurify from "dompurify";
import margin from "~/styles/modules/Margin.module.scss";
import styles from "~/styles/modules/modal/ModalText.module.scss";

interface Props {
  text?: string;
  htmlInner?: boolean;
}

export const ModalTitle: FC<Props> = memo(({ text }) => {
  return (
    <h2 className={clsx(styles.ModalTitle, margin.MarginBottom24)}>{text}</h2>
  );
});

export const ModalText: FC<Props> = ({ text, htmlInner }) => {
  if (htmlInner)
    // eslint-disable-next-line react/no-danger
    return <p dangerouslySetInnerHTML={{ __html: DOMPurify.sanitize(text) }} />;
  return (
    <p
      className={clsx(
        styles.ModalText,
        margin.MarginBottom24,
        margin.MarginTop24,
      )}
    >
      {text}
    </p>
  );
};
