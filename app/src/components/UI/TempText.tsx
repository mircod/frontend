import { FC, memo, useEffect, useState } from "react";
import styles from "~/styles/modules/Text.module.scss";

interface Props {
  temp: number | null;
  text: string;
}

const TempText: FC<Props> = ({ text, temp }) => {
  const [color, setColor] = useState("#525358");
  const style = {
    color,
  };

  useEffect(() => {
    setColor(() => {
      if (temp !== null) {
        if (temp > 37) {
          return "#E38888";
        }
        if (temp < 36.6) {
          return "#5CA5FA";
        }
      }
      return "#525358";
    });
  }, [temp]);

  return (
    <div className={styles.TableText} style={style}>
      {text}
    </div>
  );
};

export default memo(TempText);
