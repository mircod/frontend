import { ChangeEvent, FC, useRef, useState } from "react";

import { useMutation } from "react-query";
import clsx from "clsx";
import { useDispatch } from "react-redux";
import style from "~/styles/modules/search/search.module.scss";
import SearchSvg from "~/components/UI/svg/SearchSvg";
import ClearSvg from "~/components/UI/svg/ClearSvg";
import DropdownSearch from "~/components/UI/dropdown/DropdownSearch";
import useDebounce from "~/hooks/useDebounce";
import { Patient, UserService } from "~/api/generated";
import { apiInit } from "~/api/api";
import { setUsers } from "~/store/slices/users";
import useOnClickOutside from "~/hooks/useOnClickOutside";

interface SearchProps {
  type?: string;
  name?: string;
  placeholder?: string;
}

const Search: FC<SearchProps> = ({ type, name, placeholder }) => {
  apiInit();
  const [focus, setFocus] = useState(false);
  const [searchList, setSearchList] = useState([]);
  const [dropDown, setDropDown] = useState(false);
  const dropdownRef = useRef(null);
  const [debouncedInput, setDebouncedInput] = useState<string>("");
  const dispatch = useDispatch();
  const { isLoading, mutate } = useMutation(
    ["search"],
    (search: string) => UserService.userList(search),
    {
      onSuccess: (data) => {
        setSearchList(data);
      },
    },
  );
  const { mutate: mutateUser } = useMutation(
    ["setUsers"],
    (search: string) => UserService.userList(search),
    {
      onSuccess(data) {
        const resData = data.map((item) => {
          return { ...item, check: false };
        });
        dispatch(setUsers(resData));
      },
    },
  );

  useOnClickOutside(dropdownRef, () => setDropDown(false));

  const clickSearch = (item: Patient) => {
    mutateUser(item.name);
    setDebouncedInput(item.name);
    setDropDown(false);
  };

  useDebounce(
    () => {
      mutate(debouncedInput);
    },
    500,
    [debouncedInput],
  );

  const onFocus = () => {
    setFocus(true);
  };

  const onBlur = () => {
    setFocus(false);
  };

  const clearSearch = () => {
    setDebouncedInput("");
    mutateUser("");
  };

  const handleSearch = (evt: ChangeEvent<HTMLInputElement>) => {
    const keyword = evt.target.value;
    setDebouncedInput(keyword);
    setDropDown(true);
  };

  return (
    <div ref={dropdownRef} className={style.search}>
      <SearchSvg active={focus || debouncedInput.length > 0} />
      <input
        type={type}
        name={name}
        className={clsx(style.searchInput, {
          [style.searchInputActive]: debouncedInput.length > 0,
        })}
        placeholder={placeholder}
        value={debouncedInput}
        onChange={handleSearch}
        onBlur={onBlur}
        onFocus={onFocus}
      />
      <button type="button" className={style.clear} onClick={clearSearch}>
        <ClearSvg active={debouncedInput.length > 0} />
      </button>
      <DropdownSearch
        listItems={searchList}
        clickSearch={clickSearch}
        open={debouncedInput.length > 0 && dropDown}
        isLoading={isLoading}
      />
    </div>
  );
};

export default Search;
