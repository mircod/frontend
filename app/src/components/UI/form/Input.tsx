import { FC } from "react";

// eslint-disable-next-line import/no-useless-path-segments
import style from "~/styles/modules/form/input.module.scss";

interface InputProps {
  label?: string;
  labelFor?: string;
  type?: string;
  id?: string;
  name?: string;
  placeholder?: string;
  value?: string;
  error?: boolean;
  onChange?: (event: any) => void;
  onBlur?: (event: any) => void;
}

const Input: FC<InputProps> = ({
  label,
  labelFor,
  type,
  id,
  name,
  placeholder,
  value,
  error,
  onChange,
  onBlur,
}) => {
  return (
    <div
      className={
        error ? `${style.input_error} ${style.input}` : `${style.input}`
      }
    >
      <label htmlFor={labelFor}>{label}</label>
      <input
        type={type}
        id={id}
        name={name}
        placeholder={placeholder}
        value={value}
        onChange={onChange}
        onBlur={onBlur}
      />
    </div>
  );
};

export default Input;
