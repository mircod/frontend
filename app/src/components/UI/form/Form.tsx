import { FC, ReactNode } from "react";

// eslint-disable-next-line import/no-useless-path-segments
import style from "./../../../styles/modules/form/form.module.scss";

interface FormProps {
  children: ReactNode;
  onSubmit?: (event: any) => void;
}

const Form: FC<FormProps> = ({ children, onSubmit }) => {
  return (
    <form className={style.form} onSubmit={onSubmit}>
      {children}
    </form>
  );
};

export default Form;
