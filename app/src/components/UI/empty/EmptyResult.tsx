import { FC } from "react";
import style from "~/styles/modules/Empty.module.scss";

interface Props {
  text: string;
}

const EmptyResult: FC<Props> = ({ text }) => {
  return (
    <div className={style.Empty}>
      <div className={style.EmptyBlock}>
        <div className={style.EmptyBlockImg}>
          <img src="./images/icons/no-results.png" alt="" />
        </div>
        <p className={style.EmptyBlockText}>{text}</p>
      </div>
    </div>
  );
};

export default EmptyResult;
