import { FC, ReactNode } from "react";
import clsx from "clsx";
import styles from "~/styles/modules/table/GridTable.module.scss";

interface Props {
  children: ReactNode;
}

export const GridTableSix: FC<Props> = ({ children }) => {
  return (
    <div className={clsx(styles.GridTable, styles.GridTableSix)}>
      {children}
    </div>
  );
};

export const GridTableThree: FC<Props> = ({ children }) => {
  return (
    <div className={clsx(styles.GridTable, styles.GridTableThree)}>
      {children}
    </div>
  );
};
