import { FC, ReactNode } from "react";
import styles from "~/styles/modules/table/Table.module.scss";

interface Props {
  children: ReactNode;
}

const Table: FC<Props> = ({ children }) => {
  return <div className={styles.Table}>{children}</div>;
};

export default Table;
