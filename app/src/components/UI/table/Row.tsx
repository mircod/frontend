import { FC, ReactNode } from "react";
import clsx from "clsx";
import { Link } from "react-router-dom";
import styles from "~/styles/modules/table/Row.module.scss";

interface Props {
  children: ReactNode;
  borderRadiusTop?: boolean;
  borderRadiusBottom?: boolean;
  darkBackground?: boolean;
}

interface PropsLink {
  children: ReactNode;
  borderRadiusTop?: boolean;
  borderRadiusBottom?: boolean;
  darkBackground?: boolean;
  link?: string;
}

const Row: FC<Props> = ({
  children,
  borderRadiusTop = false,
  borderRadiusBottom = false,
  darkBackground = false,
}) => {
  return (
    <div
      className={clsx(styles.Row, {
        [styles.RowBorderRadiusTop]: borderRadiusTop,
        [styles.RowBorderRadiusBottom]: borderRadiusBottom,
        [styles.RowDarkBackground]: darkBackground,
      })}
    >
      {children}
    </div>
  );
};

export const RowLink: FC<PropsLink> = ({
  children,
  borderRadiusTop = false,
  borderRadiusBottom = false,
  darkBackground = false,
  link = "",
}) => {
  return (
    <div
      className={clsx(styles.Row, styles.RowRowLink, {
        [styles.RowBorderRadiusTop]: borderRadiusTop,
        [styles.RowBorderRadiusBottom]: borderRadiusBottom,
        [styles.RowDarkBackground]: darkBackground,
      })}
    >
      <Link to={link} className={styles.RowLink}>
        {children}
      </Link>
    </div>
  );
};

export default Row;
