import { FC, ReactNode } from "react";
import clsx from "clsx";
import styles from "~/styles/modules/table/Cell.module.scss";

interface Props {
  children: ReactNode;
  type?: "Checkbox" | "Id" | "Dropdown" | "";
}

const Cell: FC<Props> = ({ children, type = "" }) => {
  return (
    <div className={clsx(styles.Cell, styles[`Cell${type}`])}>{children}</div>
  );
};

export default Cell;
