import { FC, memo } from "react";
import clsx from "clsx";
import styles from "~/styles/modules/Text.module.scss";

interface Props {
  text?: string | number;
  textTitle?: boolean;
  textCenter?: boolean;
  textBreak?: boolean;
}

const TableText: FC<Props> = ({ text, textTitle, textCenter, textBreak }) => {
  return (
    <div
      className={clsx(styles.TableText, {
        [styles.TableTextCenter]: textCenter,
        [styles.TableTextTitle]: textTitle,
        [styles.TableTextBreak]: textBreak,
      })}
    >
      {text}
    </div>
  );
};

export default memo(TableText);
