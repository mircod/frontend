import { FC, memo } from "react";
import clsx from "clsx";
import styles from "~/styles/modules/Skeleton.module.scss";

interface Props {
  height24?: boolean;
}

const Skeleton: FC<Props> = ({ height24 }) => {
  return (
    <div
      className={clsx(styles.Skeleton, styles.SkeletonWave, {
        [styles.SkeletonHeight24]: height24,
      })}
    />
  );
};

export default memo(Skeleton);
