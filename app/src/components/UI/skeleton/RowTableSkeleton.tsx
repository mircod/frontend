import { FC } from "react";
import Row from "~/components/UI/table/Row";
import { GridTableSix, GridTableThree } from "~/components/UI/table/GridTable";
import Cell from "~/components/UI/table/Cell";
import CheckBox from "~/components/UI/CheckBox";
import Skeleton from "~/components/UI/skeleton/Skeleton";

interface Props {
  last: boolean;
}

export const RowTableSkeletonSix: FC<Props> = ({ last }) => {
  return (
    <Row borderRadiusBottom={last}>
      <GridTableSix>
        <Cell>
          <Skeleton />
        </Cell>
        <Cell>
          <Skeleton />
        </Cell>
        <Cell>
          <Skeleton />
        </Cell>
        <Cell>
          <Skeleton />
        </Cell>
        <Cell>
          <Skeleton />
        </Cell>
        <Cell type="Checkbox">
          <CheckBox />
        </Cell>
      </GridTableSix>
    </Row>
  );
};

export const RowTableSkeletonThree: FC<Props> = ({ last }) => {
  return (
    <Row borderRadiusBottom={last}>
      <GridTableThree>
        <Cell>
          <Skeleton />
        </Cell>
        <Cell>
          <Skeleton />
        </Cell>
        <Cell>
          <Skeleton />
        </Cell>
      </GridTableThree>
    </Row>
  );
};
