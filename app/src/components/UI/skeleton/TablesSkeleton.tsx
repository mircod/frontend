import { FC } from "react";
import {
  RowTableSkeletonSix,
  RowTableSkeletonThree,
} from "~/components/UI/skeleton/RowTableSkeleton";

export const TablesSkeletonSix: FC = () => {
  return (
    <>
      {[...Array(8)].map((e, i) => {
        // eslint-disable-next-line react/no-array-index-key
        return <RowTableSkeletonSix key={`${e}_${i}`} last={i === 7} />;
      })}
    </>
  );
};

export const TableSkeletonThree = () => {
  return (
    <>
      {[...Array(3)].map((e, i) => {
        // eslint-disable-next-line react/no-array-index-key
        return <RowTableSkeletonThree key={`${e}_${i}`} last={i === 2} />;
      })}
    </>
  );
};
