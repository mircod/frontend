import { FC } from "react";
import Skeleton from "~/components/UI/skeleton/Skeleton";

const SearchSkeleton: FC = () => {
  return (
    <>
      {[...Array(3)].map((e, i) => {
        // eslint-disable-next-line react/no-array-index-key
        return <Skeleton height24 key={`${e}__${i}`} />;
      })}
    </>
  );
};

export default SearchSkeleton;
