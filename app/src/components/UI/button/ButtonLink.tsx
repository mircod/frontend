import { FC, memo } from "react";
import styles from "~/styles/modules/button/ButtonLink.module.scss";

interface Props {
  text: boolean;
  onClick: () => void;
}

const ButtonLink: FC<Props> = ({ text, onClick }) => {
  return (
    <button type="button" className={styles.ButtonLink} onClick={onClick}>
      {text}
    </button>
  );
};

export default memo(ButtonLink);
