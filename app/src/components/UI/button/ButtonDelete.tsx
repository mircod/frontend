import { FC, memo } from "react";
import clsx from "clsx";
import TrashSvg from "~/components/UI/svg/TrashSvg";
import styles from "~/styles/modules/button/Trash.module.scss";

interface Props {
  active?: boolean;
  onClick: () => void;
}

const ButtonDelete: FC<Props> = ({ active, onClick }) => {
  return (
    <button
      type="button"
      disabled={!active}
      className={clsx(styles.TrashBtn, { [styles.TrashBtnActive]: active })}
      onClick={onClick}
    >
      <TrashSvg active={active} />
    </button>
  );
};

export default memo(ButtonDelete);
