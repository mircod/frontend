import { ButtonHTMLAttributes, DetailedHTMLProps, FC } from "react";
import clsx from "clsx";
import style from "~/styles/modules/button/button.module.scss";
import LoaderSvg from "~/components/UI/svg/LoaderSvg";

interface ButtonProps
  extends DetailedHTMLProps<
    ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
  > {
  text: string;
  isLoading?: boolean;
  smallLoader?: boolean;
  className?: string;
  onClick?: () => void;
}

const Button: FC<ButtonProps> = ({
  text,
  isLoading,
  className,
  smallLoader,
  onClick,
  ...rest
}) => {
  return (
    // eslint-disable-next-line react/button-has-type
    <button
      className={clsx(style.button, className)}
      onClick={onClick}
      disabled={isLoading}
      {...rest}
    >
      {isLoading ? <LoaderSvg small={smallLoader} /> : <div>{text}</div>}
    </button>
  );
};

export default Button;
