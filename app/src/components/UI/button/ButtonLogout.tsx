import { FC } from "react";
import { useNavigate } from "react-router-dom";
import styles from "~/styles/modules/button/ButtonLogout.module.scss";
import ExitSvg from "~/components/UI/svg/ExitSvg";
import { LOGIN } from "~/constants/route";
import { isHaveToken } from "~/api/api";

const ButtonLogout: FC = () => {
  const token = isHaveToken();
  const navigate = useNavigate();
  const logout = () => {
    localStorage.removeItem("token");
    navigate(LOGIN);
  };

  if (!token) return null;

  return (
    <button type="button" className={styles.ButtonLogout} onClick={logout}>
      <ExitSvg />
    </button>
  );
};

export default ButtonLogout;
