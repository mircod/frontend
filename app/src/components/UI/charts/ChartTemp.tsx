import { FC } from "react";
// @ts-ignore
import minBy from "lodash.minby";
// @ts-ignore
import maxBy from "lodash.maxby";
import {
  ResponsiveContainer,
  AreaChart,
  XAxis,
  YAxis,
  Tooltip,
  Area,
  CartesianGrid,
  TooltipProps,
} from "recharts";
import {
  NameType,
  ValueType,
} from "recharts/types/component/DefaultTooltipContent";
import { useSelector } from "react-redux";
import styles from "~/styles/modules/CustomTooltip.module.scss";
import { Measurement } from "~/api/generated";
import { selectGetTemp } from "~/store/slices/user";
import { dateFormatterChart } from "~/utils/dateFormatter";

interface CustomTooltip {
  active: boolean;
  payload: any;
  label: any;
}

const ChartTemp: FC = () => {
  const tempData: Array<Measurement> = useSelector(selectGetTemp);

  const arrayMin = (arr: Measurement[]) => {
    return minBy(arr, (item: Measurement) => item.value);
  };

  const arrayMax = (arr: Measurement[]) => {
    return maxBy(arr, (item: Measurement) => item.value);
  };

  return (
    <ResponsiveContainer width="100%" height={365}>
      <AreaChart data={tempData}>
        <defs>
          <linearGradient id="colorTemp" x1="0" y1="0" x2="1" y2="0">
            <stop offset="4.88%" stopColor="#F3F0FF" stopOpacity={1} />
            <stop offset="101.76%" stopColor="#F8DED4" stopOpacity={1} />
          </linearGradient>
          <linearGradient id="colorStroke" x1="0" y1="0" x2="1" y2="0">
            <stop offset="3.84%" stopColor="#10A7C8" stopOpacity={1} />
            <stop offset="49.99%" stopColor="#8585BA" stopOpacity={1} />
            <stop offset="91.21%" stopColor="#F08C83" stopOpacity={1} />
          </linearGradient>
        </defs>
        <XAxis
          dy={10}
          axisLine={false}
          tickLine={false}
          dataKey="date"
          tickFormatter={dateFormatterChart}
        />
        <YAxis
          dx={-20}
          axisLine={false}
          tickLine={false}
          dataKey="value"
          domain={[arrayMin(tempData).value, arrayMax(tempData).value]}
        />
        <Tooltip content={CustomTooltip} />
        <Area
          type="monotone"
          dataKey="value"
          strokeWidth="2.5px"
          stroke="url(#colorStroke)"
          fillOpacity={1}
          fill="url(#colorTemp)"
        />
        <CartesianGrid
          widths="0.5px"
          color="#E4EAF0"
          opacity={0.4}
          vertical={false}
        />
      </AreaChart>
    </ResponsiveContainer>
  );
};

// eslint-disable-next-line @typescript-eslint/no-redeclare
function CustomTooltip({
  active,
  payload,
  label,
}: TooltipProps<ValueType, NameType>) {
  const setColor = () => {
    if (payload.length > 0) {
      const temp = payload[0].value;
      if (temp !== null) {
        if (temp > 37) {
          return "#E38888";
        }
        if (temp < 36.6) {
          return "#5CA5FA";
        }
      }
    }
    return "#525358";
  };

  const style = {
    color: setColor(),
  };

  if (active) {
    return (
      <div className={styles.tooltip}>
        <h4 className={styles.tooltipTitle}>{dateFormatterChart(label)}</h4>
        <p className={styles.tooltipTemp} style={style}>
          {payload[0].value}°C
        </p>
      </div>
    );
  }
  return null;
}

export default ChartTemp;
