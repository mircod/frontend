import { FC } from "react";
import clsx from "clsx";
import styles from "~/styles/modules/CheckBox.module.scss";
import CheckSvg from "~/components/UI/svg/CheckSvg";

interface Props {
  value?: Boolean;
  small?: Boolean;
  check?: any;
  dark?: Boolean;
  name?: string;
}

const CheckBox: FC<Props> = ({ value, small, check, dark, name }) => {
  return (
    <button
      name={name}
      type="button"
      className={clsx(styles.CheckBox, {
        [styles.CheckBoxSmall]: small,
        [styles.CheckBoxChecked]: value,
      })}
      onClick={check}
    >
      <div
        className={clsx(styles.CheckBoxBox, {
          [styles.CheckBoxBoxSmall]: small,
          [styles.CheckBoxBoxDark]: dark,
        })}
      >
        <CheckSvg />
      </div>
    </button>
  );
};

export default CheckBox;
