import { FC, useEffect, useRef, useState } from "react";
import clsx from "clsx";
import { useTranslation } from "react-i18next";
import style from "~/styles/modules/select/SelectLanguage.module.scss";
import { LANGUAGES } from "~/constants/languages";
import DownArrowSvg from "~/components/UI/svg/DownArrowSvg";
import useOnClickOutside from "~/hooks/useOnClickOutside";
import BrowserLanguage from "~/utils/BrowserLanguage";

interface Language {
  key: string;
  name: string;
}

const SelectLanguage: FC = () => {
  const [languages] = useState(LANGUAGES);
  const [language, setLanguage] = useState(LANGUAGES[0].name);
  const [active, setActive] = useState(false);
  const { i18n } = useTranslation();
  const languageRef = useRef(null);

  useOnClickOutside(languageRef, () => setActive(false));

  useEffect(() => {
    const lang = BrowserLanguage.getDefaultLanguage();
    const findLang = LANGUAGES.find((item) => item.key === lang);
    if (findLang) {
      setLanguage(findLang.name);
    }
  }, []);

  const changeLanguage = (value: Language) => {
    setActive(false);
    setLanguage(value.name);
    i18n.changeLanguage(value.key);
    BrowserLanguage.setLanguage(value.key);
  };

  const changeActive = () => {
    setActive(!active);
  };

  return (
    <div ref={languageRef} className={style.Select}>
      <button
        type="button"
        className={clsx(style.SelectBlock, { [style.SelectBlockOpen]: active })}
        onClick={() => changeActive()}
      >
        <div>{language}</div>
        <DownArrowSvg />
      </button>
      <nav
        className={clsx(style.SelectSelectList, {
          [style.SelectSelectListOpen]: active,
        })}
      >
        {languages.map((item) => {
          if (item.key === language) return "";
          return (
            <button
              type="button"
              key={item.key}
              className={style.SelectSelectItem}
              onClick={() => changeLanguage(item)}
            >
              {item.name}
            </button>
          );
        })}
      </nav>
    </div>
  );
};

export default SelectLanguage;
