import { FC, ReactNode } from "react";
// eslint-disable-next-line import/no-useless-path-segments
import styles from "./../styles/modules/Container.module.scss";

interface Props {
  children: ReactNode;
}

const Container: FC<Props> = ({ children }) => {
  return <div className={styles.Container}>{children}</div>;
};

export default Container;
