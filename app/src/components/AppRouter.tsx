import { Navigate, Route, Routes } from "react-router-dom";
import MainLayout from "~/layouts/MainLayout";
import Home from "~/pages/Home";
import Login from "~/pages/Login";
import SignUp from "~/pages/SignUp";
import PageNotFound from "~/pages/PageNotFound";
import { HOME, LOGIN, SIGNUP, ERROR_404, USER_CARD } from "~/constants/route";
import UserCard from "~/pages/UserCard";

function AppRouter() {
  return (
    <Routes>
      <Route path={HOME} element={<MainLayout />}>
        <Route path="*" element={<Navigate to={ERROR_404} />} />
        <Route index element={<Home />} />
        <Route path={USER_CARD} element={<UserCard />} />
        <Route path={LOGIN} element={<Login />} />
        <Route path={SIGNUP} element={<SignUp />} />
        <Route path={ERROR_404} element={<PageNotFound />} />
      </Route>
    </Routes>
  );
}

export default AppRouter;
