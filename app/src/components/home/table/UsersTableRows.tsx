import { FC } from "react";
import { useSelector } from "react-redux";
import UsersTableRow from "~/components/home/table/UsersTableRow";
import { TablesSkeletonSix } from "~/components/UI/skeleton/TablesSkeleton";
import { selectGetUsers } from "~/store/slices/users";
import { User } from "~/appTypes/users";
import useUsers from "~/hooks/useUsers";

const UsersTableRows: FC = () => {
  const { isLoading } = useUsers();
  const users: Array<User> = useSelector(selectGetUsers);

  if (isLoading) return <TablesSkeletonSix />;

  return (
    <div>
      {users.map((user: User, index: number) => (
        <UsersTableRow
          user={user}
          key={user.id}
          last={index === users.length - 1}
        />
      ))}
    </div>
  );
};

export default UsersTableRows;
