import { FC } from "react";
import Table from "~/components/UI/table/Table";
import UsersTableHeader from "~/components/home/table/UsersTableHeader";
import UsersTableRows from "~/components/home/table/UsersTableRows";

const UsersTable: FC = () => (
  <Table>
    <UsersTableHeader />
    <UsersTableRows />
  </Table>
);

export default UsersTable;
