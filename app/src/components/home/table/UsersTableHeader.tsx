import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { GridTableSix } from "~/components/UI/table/GridTable";
import Row from "~/components/UI/table/Row";
import Cell from "~/components/UI/table/Cell";
import styles from "~/styles/modules/svg/FilterSvg.module.scss";
import CheckBox from "~/components/UI/CheckBox";
import TableText from "~/components/UI/table/TableText";
import { selectIsCheckAll, setCheckAll } from "~/store/slices/users";

const UsersTableHeader = () => {
  const [value, setValue] = useState(false);
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const isSelectAll = useSelector(selectIsCheckAll);

  useEffect(() => {
    setValue(isSelectAll);
  }, [isSelectAll]);

  const checkAll = () => {
    setValue(!value);
    dispatch(setCheckAll({ flag: !value }));
  };

  return (
    <Row borderRadiusTop darkBackground>
      <GridTableSix>
        <Cell type="Id">
          <span>#</span>
        </Cell>
        <Cell>
          <TableText text={t("ФИО или табельный номер")} textTitle />
        </Cell>
        <Cell type="Dropdown">
          <div className={styles.FilterDiv}>
            <TableText text={t("Температура")} textTitle />
            {/* Todo когда нибудь в следующий раз... */}
            {/* <div ref={dropdownRef} className={styles.FilterDivBlock}> */}
            {/*  <button */}
            {/*    type="button" */}
            {/*    className={styles.FilterDivButton} */}
            {/*    onClick={(e) => { */}
            {/*      e.stopPropagation(); */}
            {/*      clickFilter(); */}
            {/*    }} */}
            {/*  > */}
            {/*    <FilterSvg active={filter} /> */}
            {/*  </button> */}
            {/*  <Dropdown open={filter} /> */}
            {/* </div> */}
          </div>
        </Cell>
        <Cell>
          <TableText text={t("UID устр-в")} textTitle />
        </Cell>
        <Cell>
          <TableText text={t("Заряд")} textTitle />
        </Cell>
        <Cell type="Checkbox">
          <CheckBox value={value} dark check={checkAll} />
        </Cell>
      </GridTableSix>
    </Row>
  );
};

export default UsersTableHeader;
