import { ChangeEvent, FC } from "react";
import { useTranslation } from "react-i18next";
import { useDispatch } from "react-redux";
import { GridTableSix } from "~/components/UI/table/GridTable";
import Cell from "~/components/UI/table/Cell";
import CheckBox from "~/components/UI/CheckBox";
import { RowLink } from "~/components/UI/table/Row";
import Charge from "~/components/UI/Charge";
import TempText from "~/components/UI/TempText";
import TableText from "~/components/UI/table/TableText";
import { PayloadUserCheck, User } from "~/appTypes/users";
import { setCheckUser } from "~/store/slices/users";
import { dateFormatter } from "~/utils/dateFormatter";

interface Props {
  user: User;
  last: boolean;
}

const UsersTableRow: FC<Props> = ({ user, last }) => {
  // Визуальное решение без логики
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const check = (payload: PayloadUserCheck) => {
    dispatch(setCheckUser(payload));
  };

  return (
    <RowLink
      link={`card/${user.id}`}
      borderRadiusBottom={last}
      darkBackground={user.check}
    >
      <GridTableSix>
        <Cell type="Id">
          <TableText text={user.id} textCenter />
        </Cell>
        <Cell>
          <TableText text={user.name} />
        </Cell>
        <Cell>
          <TempText
            temp={user.temperature}
            text={
              user.temperature
                ? `${user.temperature} °C (${dateFormatter(user.temp_date)})`
                : t("нет данных")
            }
          />
        </Cell>
        <Cell>
          <TableText text={user.device_id ? user.device_id : "—"} textBreak />
        </Cell>
        <Cell>
          <Charge charge={user.charge} />
        </Cell>
        <Cell type="Checkbox">
          <CheckBox
            value={user.check}
            check={(e: ChangeEvent) => {
              e.preventDefault();
              check({ id: user.id });
            }}
          />
        </Cell>
      </GridTableSix>
    </RowLink>
  );
};

export default UsersTableRow;
