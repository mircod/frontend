import { FC, useLayoutEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { useMutation, useQueryClient } from "react-query";
import styles from "~/styles/modules/button/button.module.scss";
import margin from "~/styles/modules/Margin.module.scss";
import Modal from "~/components/UI/modal/Modal";
import { ModalText } from "~/components/UI/modal/ModalText";
import Button from "~/components/UI/button/Button";
import { clearModal, selectComponentModal } from "~/store/slices/modal";
import { selectCheckCounter } from "~/store/slices/users";
import getNoun from "~/utils/changeNouns";
import { BulkDeleteUser, UserService } from "~/api/generated";
import { apiInit } from "~/api/api";

const ModalConfirmDelete: FC = () => {
  const [text, setText] = useState("");
  const [disable, setDisable] = useState(false);
  const { t } = useTranslation();
  const { open } = useSelector(selectComponentModal);
  const { counter, info } = useSelector(selectCheckCounter);
  const dispatch = useDispatch();
  const queryClient = useQueryClient();
  const { mutate } = useMutation(
    ["deleteUsers"],
    (ids: BulkDeleteUser) => UserService.userBulkDelete(ids),
    {
      onSuccess: () => {
        dispatch(clearModal({ open: false }));
        setDisable(false);
        queryClient.invalidateQueries(["users"]);
      },
    },
  );
  apiInit();

  const fillText = () => {
    if (counter > 1) {
      setText(
        `${t(
          "Вы уверены, что хотите удалить",
        )} <span style="font-weight: 600;">${counter}</span> ${getNoun(
          counter,
          [t("пользователь"), t("пользователей"), t("пользователей")],
        )}?`,
      );
    } else if (counter === 1) {
      setText(
        `${t(
          "Вы уверены, что хотите удалить пользователя",
        )} <span style="font-weight: 600;">${info[0].name}</span>?`,
      );
    }
  };

  const deleteUser = () => {
    const idsUser = info.map((item) => item.id);
    mutate({ id: idsUser });
    setDisable(true);
  };

  useLayoutEffect(() => {
    fillText();
  }, [open]);

  const clickModal = () => {
    dispatch(clearModal({ open: false }));
  };

  return (
    <Modal active={open}>
      <ModalText htmlInner text={text} />
      <ModalText text={t("Действите нельзя будет отменить")} />
      <Button
        type="button"
        text={t("Подтвердить")}
        className={styles.buttonConfirm}
        onClick={deleteUser}
        isLoading={disable}
        smallLoader
      />
      <div className={margin.MarginBottom16} />
      <Button
        type="button"
        text={t("Отменить")}
        disabled={disable}
        className={styles.buttonCancel}
        onClick={clickModal}
      />
    </Modal>
  );
};

export default ModalConfirmDelete;
