import { FC } from "react";
import style from "~/styles/modules/footer/UsersFooter.module.scss";
import UsersChooseText from "~/components/home/footer/UsersChooseText";

const UsersFooter: FC = () => {
  return (
    <div className={style.UsersFooter}>
      <div className={style.UsersFooterDynamicText}>
        <UsersChooseText />
      </div>
      <div className={style.UsersFooterPaginationBlock} />
    </div>
  );
};

export default UsersFooter;
