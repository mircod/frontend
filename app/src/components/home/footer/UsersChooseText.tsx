import { FC } from "react";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import ButtonLink from "~/components/UI/button/ButtonLink";
import { selectCheckCounter, setCheckAll } from "~/store/slices/users";
import getNoun from "~/utils/changeNouns";
import styles from "~/styles/modules/footer/UsersChooseText.module.scss";

const UsersChooseText: FC = () => {
  const { t } = useTranslation();
  const { counter } = useSelector(selectCheckCounter);
  const dispatch = useDispatch();

  const cancelChoose = () => {
    dispatch(setCheckAll({ flag: false }));
  };

  if (counter === 0) return null;

  return (
    <div className={styles.UsersChooseText}>
      <span className={styles.UsersChooseTextText}>
        {t("Выбрано")}:
        <span>
          {" "}
          {counter}{" "}
          {getNoun(counter, [
            t("пользователь"),
            t("пользователя"),
            t("пользователей"),
          ])}
        </span>
      </span>
      <ButtonLink text={t("Снять выделение")} onClick={cancelChoose} />
    </div>
  );
};

export default UsersChooseText;
