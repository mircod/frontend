import { FC } from "react";
import style from "~/styles/modules/home/UsersHeader.module.scss";
import FullLogo from "~/components/UI/logo/FullLogo";
import UsersActions from "~/components/home/header/UsersActions";

const UsersHeader: FC = () => {
  return (
    <div className={style.UsersHeader}>
      <FullLogo />
      <UsersActions />
    </div>
  );
};

export default UsersHeader;
