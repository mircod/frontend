import { FC } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation } from "react-i18next";
import style from "~/styles/modules/home/UsersHeader.module.scss";
import ButtonDelete from "~/components/UI/button/ButtonDelete";
import { selectCheckCounter } from "~/store/slices/users";
import { setOpen } from "~/store/slices/modal";
import Search from "~/components/UI/search/Search";

const UsersActions: FC = () => {
  const { counter } = useSelector(selectCheckCounter);
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const openModal = () => {
    dispatch(setOpen({ open: true, component: "ModalConfirmDelete" }));
  };

  return (
    <div className={style.UsersHeaderActionBlock}>
      <Search name="search" type="text" placeholder={t("Поиск")} />
      <ButtonDelete active={counter > 0} onClick={openModal} />
    </div>
  );
};

export default UsersActions;
