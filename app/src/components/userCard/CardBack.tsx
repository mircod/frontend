import { FC, memo } from "react";
import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { HOME } from "~/constants/route";
import styles from "~/styles/modules/cardUser/CardBack.module.scss";
import ArrowLeft from "~/components/UI/svg/ArrowLeft";

const CardBack: FC = () => {
  const { t } = useTranslation();
  return (
    <Link to={HOME} className={styles.CardBack}>
      <ArrowLeft />
      <span>{t("к списку пользователей")}</span>
    </Link>
  );
};

export default memo(CardBack);
