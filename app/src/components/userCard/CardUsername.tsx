import { FC } from "react";
import styles from "~/styles/modules/cardUser/CardUsername.module.scss";

interface Props {
  name: string;
}

const CardUsername: FC<Props> = ({ name }) => {
  return <div className={styles.CardUsername}>{name}</div>;
};

export default CardUsername;
