import { FC } from "react";
import clsx from "clsx";
import styles from "~/styles/modules/Margin.module.scss";
import Table from "~/components/UI/table/Table";
import CardTableHeader from "~/components/userCard/table/CardTableHeader";
import CardTableRows from "~/components/userCard/table/CardTableRows";

const CardTable: FC = () => {
  return (
    <div className={clsx(styles.MarginTop40, styles.MarginBottom64)}>
      <Table>
        <CardTableHeader />
        <CardTableRows />
      </Table>
    </div>
  );
};

export default CardTable;
