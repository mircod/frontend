import { FC } from "react";
import { useTranslation } from "react-i18next";
import { GridTableThree } from "~/components/UI/table/GridTable";
import Cell from "~/components/UI/table/Cell";
import Row from "~/components/UI/table/Row";
import TempText from "~/components/UI/TempText";
import TableText from "~/components/UI/table/TableText";
import { Measurement } from "~/api/generated";
import { dateFormatter } from "~/utils/dateFormatter";

interface Props {
  temp: Measurement;
  last: boolean;
}

const CardTableRow: FC<Props> = ({ temp, last }) => {
  const { t } = useTranslation();
  return (
    <Row borderRadiusBottom={last}>
      <GridTableThree>
        <Cell>
          <TableText text={dateFormatter(temp.date)} />
        </Cell>
        <Cell>
          <TempText
            temp={temp.value}
            text={temp.value ? `${temp.value} °C` : t("нет данных")}
          />
        </Cell>
        <Cell>
          <TableText text={temp.device ? temp.device : "—"} textBreak />
        </Cell>
      </GridTableThree>
    </Row>
  );
};
export default CardTableRow;
