import { FC } from "react";
import { useSelector } from "react-redux";
import { TableSkeletonThree } from "~/components/UI/skeleton/TablesSkeleton";
import CardTableRow from "~/components/userCard/table/CardTableRow";
import { Measurement } from "~/api/generated";
import { selectGetTemp } from "~/store/slices/user";

const CardTableRows: FC = () => {
  // Todo заменить на реальный isLoading
  const isLoading = false;
  const tempData: Array<Measurement> = useSelector(selectGetTemp);

  if (isLoading) return <TableSkeletonThree />;

  return (
    <div>
      {tempData.map((item: Measurement, index: number) => (
        <CardTableRow
          temp={item}
          key={item.id}
          last={index === tempData.length - 1}
        />
      ))}
    </div>
  );
};

export default CardTableRows;
