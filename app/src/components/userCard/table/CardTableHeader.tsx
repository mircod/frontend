import { FC } from "react";
import { useTranslation } from "react-i18next";
import Row from "~/components/UI/table/Row";
import { GridTableThree } from "~/components/UI/table/GridTable";
import Cell from "~/components/UI/table/Cell";
import TableText from "~/components/UI/table/TableText";

const CardTableHeader: FC = () => {
  const { t } = useTranslation();
  return (
    <Row borderRadiusTop darkBackground>
      <GridTableThree>
        <Cell>
          <TableText text={t("Дата")} textTitle />
        </Cell>
        <Cell>
          <TableText text={t("Измерение")} textTitle />
        </Cell>
        <Cell>
          <TableText text={t("Устройство")} textTitle />
        </Cell>
      </GridTableThree>
    </Row>
  );
};

export default CardTableHeader;
