import { FC } from "react";
import clsx from "clsx";
import { useSelector } from "react-redux";
import styles from "~/styles/modules/cardUser/CardInfo.module.scss";
import margin from "~/styles/modules/Margin.module.scss";
import CardUsername from "~/components/userCard/CardUsername";
import DeviceInfo from "~/components/userCard/device/DeviceInfo";
import { selectGetUser } from "~/store/slices/user";

const CardInfo: FC = () => {
  const data = useSelector(selectGetUser);
  return (
    <div className={clsx(styles.CardInfo, margin.MarginBottom40)}>
      <CardUsername name={data.name} />
      <DeviceInfo />
    </div>
  );
};

export default CardInfo;
