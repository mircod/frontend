import { FC, memo } from "react";
import { useTranslation } from "react-i18next";
import styles from "~/styles/modules/cardUser/device/DeviceTempDate.module.scss";

interface Props {
  date: string;
}

const DeviceTempDate: FC<Props> = ({ date }) => {
  const { t } = useTranslation();
  return (
    <div className={styles.DeviceTempDate}>
      {t("Обновлено")} {date}
    </div>
  );
};

export default memo(DeviceTempDate);
