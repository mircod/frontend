import { FC } from "react";
import { useSelector } from "react-redux";
import styles from "~/styles/modules/cardUser/device/DeviceInfo.module.scss";
import DeviceStatus from "~/components/userCard/device/DeviceStatus";
import DeviceTemp from "~/components/userCard/device/DeviceTemp";
import { selectGetUser } from "~/store/slices/user";

const DeviceInfo: FC = () => {
  const data = useSelector(selectGetUser);
  return (
    <div className={styles.DeviceInfo}>
      <div className={styles.DeviceInfoFlex}>
        <DeviceStatus charge={data.charge} />
        {data.temperature !== "" ? <DeviceTemp temp={data.temperature} /> : ""}
      </div>
    </div>
  );
};

export default DeviceInfo;
