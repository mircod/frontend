import { FC } from "react";
import DeviceStatusItem from "~/components/userCard/device/DeviceStatusItem";
import StatusSvg from "~/components/UI/svg/StatusSvg";
import Bluetooth from "~/components/UI/svg/Bluetooth";
import Charge from "~/components/UI/Charge";
import styles from "~/styles/modules/cardUser/device/DeviceStatus.module.scss";

interface Props {
  charge: number | null;
}

const DeviceStatus: FC<Props> = ({ charge }) => {
  return (
    <div className={styles.DeviceStatus}>
      <DeviceStatusItem>
        <StatusSvg />
      </DeviceStatusItem>
      <DeviceStatusItem>
        <Bluetooth />
      </DeviceStatusItem>
      <DeviceStatusItem>
        <Charge charge={charge} />
      </DeviceStatusItem>
    </div>
  );
};

export default DeviceStatus;
