import { FC } from "react";
import styles from "~/styles/modules/cardUser/device/DeviceTemp.module.scss";

interface Props {
  temp: number;
}

const DeviceTemp: FC<Props> = ({ temp }) => {
  return <div className={styles.DeviceTemp}>{temp}°C</div>;
};

export default DeviceTemp;
