import { FC, ReactNode } from "react";
import styles from "~/styles/modules/cardUser/device/DeviceStatusItem.module.scss";

interface Props {
  children: ReactNode;
}

const DeviceStatusItem: FC<Props> = ({ children }) => {
  return (
    <div className={styles.DeviceStatusItem}>
      {children}
      <div className={styles.DeviceStatusItemLine} />
    </div>
  );
};

export default DeviceStatusItem;
