import { FC } from "react";

// eslint-disable-next-line import/no-useless-path-segments
import style from "~/styles/modules/header/header.module.scss";

import Container from "../Container";
import SelectLanguage from "~/components/UI/select/SelectLanguage";
import ButtonLogout from "~/components/UI/button/ButtonLogout";

const Header: FC = () => {
  return (
    <header className={style.header}>
      <Container>
        <div className={style.header__block}>
          <h1 className={style.header__title}>TEMPCOD</h1>
          <div className={style.header__actionBlock}>
            <SelectLanguage />
            <ButtonLogout />
          </div>
        </div>
      </Container>
    </header>
  );
};

export default Header;
