import axios from "axios";

const signUpPost = (values, url) => {
  axios
    .post(url, {
      body: values,
    })
    .then((response) => {
      console.log(response);
    });
};

export default signUpPost;
