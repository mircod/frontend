import { LOCAL_STORAGE_LANG_KEY, LANGUAGES } from "../constants/languages";

const BrowserLanguage = {
  getBrowserLanguage() {
    return navigator.language || navigator.userLanguage;
  },

  getPrevLanguage() {
    return localStorage ? localStorage.getItem(LOCAL_STORAGE_LANG_KEY) : null;
  },

  setLanguage(lang) {
    if (localStorage) {
      localStorage.setItem(LOCAL_STORAGE_LANG_KEY, lang);
      return true;
    }
    return false;
  },

  getDefaultLanguage() {
    const langSet = this.getPrevLanguage();
    if (langSet) {
      return langSet;
    }
    const browserLang = this.getBrowserLanguage();
    if (browserLang) {
      let lang = "";
      LANGUAGES.forEach((item) => {
        if (item && browserLang.includes(item.key)) {
          lang = lang.key;
        }
      });
      return lang || "ru";
    }
    return "ru";
  },
};

export default BrowserLanguage;
