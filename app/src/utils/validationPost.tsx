import * as yup from "yup";
import i118n from "~/config/i118n";

const { t } = i118n;

const validationPostSchema = yup.object().shape({
  username: yup
    .string()
    .typeError(t("Должно быть строкой"))
    .required(t("Обязательное поле")),
  password: yup
    .string()
    .typeError(t("Неправильный пароль"))
    .required(t("Обязательное поле")),
  confirm_password: yup
    .string()
    .oneOf([yup.ref("password")], t("Пароли не совпадают"))
    .required(t("Обязательное поле")),
  agree: yup.boolean().required(t("Обязательное поле")),
});

export const validationLoginSchema = yup.object().shape({
  username: yup
    .string()
    .typeError(t("Должно быть строкой"))
    .required(t("Обязательное поле")),
  password: yup
    .string()
    .typeError(t("Неправильный пароль"))
    .required(t("Обязательное поле")),
});

export default validationPostSchema;
