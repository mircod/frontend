import moment from "moment";
import BrowserLanguage from "~/utils/BrowserLanguage";

export const dateFormatterChart = (item: string) => moment(item).format("H:mm");

export const dateFormatter = (date: string) => {
  const CURRENT_LANG = BrowserLanguage.getDefaultLanguage();
  const parseDate = new Date(Date.parse(date));
  return parseDate.toLocaleString(CURRENT_LANG, {
    day: "numeric",
    month: "long",
    year: "numeric",
    hour: "numeric",
    minute: "numeric",
  });
};
