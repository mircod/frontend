export const HOME = "/";
export const LOGIN = "/login";
export const ERROR_404 = "/404";
export const USER_CARD = "/card/:id";
export const SIGNUP = "sign_up";
