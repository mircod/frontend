export const IS_PRODUCTION = process.env.NODE_ENV === "production";
export const IS_DEVELOPMENT = process.env.NODE_ENV === "development";

export const APP_NAME = "Mircod";
export const DESCRIPTION = "Mircod";
export const API_URL = "http://3.67.201.254/api/v1";
