export const LOCAL_STORAGE_LANG_KEY = "RC_LANG_KEY";
export const LANGUAGES = [
  { key: "ru", name: "ru" },
  { key: "en", name: "en" },
];
