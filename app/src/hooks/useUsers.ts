import { useQuery } from "react-query";
import { UserService } from "~/api/generated";
import { apiInit } from "~/api/api";
import store from "~/store";
import { setUsers } from "~/store/slices/users";

export default function useUsers(search: string = "") {
  apiInit();
  return useQuery(["users", search], () => UserService.userList(search), {
    onSuccess(data) {
      const resData = data.map((item) => {
        return { ...item, check: false };
      });
      store.dispatch(setUsers(resData));
    },
  });
}
