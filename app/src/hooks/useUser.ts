import { useQuery } from "react-query";
import { apiInit } from "~/api/api";
import { UserService } from "~/api/generated";
import store from "~/store";
import { setTemp, setUser } from "~/store/slices/user";

export default function useUser(id: number) {
  apiInit();
  return useQuery(["user"], () => UserService.userRead(id), {
    onSuccess: (data) => {
      store.dispatch(setUser(data));
    },
  });
}

export function useUserTemp(id: string) {
  apiInit();
  return useQuery("temp", () => UserService.userTempList(id), {
    onSuccess: (data) => {
      store.dispatch(setTemp(data));
    },
  });
}
