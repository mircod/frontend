import { DependencyList, EffectCallback, useEffect } from "react";

const useDebounce = (
  effect: EffectCallback,
  delay: number,
  deps?: DependencyList,
): void => {
  useEffect(() => {
    const handler = setTimeout(() => effect(), delay);

    return () => clearTimeout(handler);
  }, [...(deps || []), delay]);
};

export default useDebounce;
