# Frontend

## Getting started

## Build Setup

```bash
# install dependencies
$ npm install

# move to one of directories:
$ cd app

# init locales files
$ npm run make-locales

# serve with hot reload at localhost:3000
$ npm run start

# build for production and launch server
$ npm run build
$ serve -s build 

# запуск storybook localhost:6006
$ npm run storybook

# сгенерировать api 
$ npm run generate-api
```